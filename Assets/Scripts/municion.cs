﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class municion : MonoBehaviour {


	//ZONA DE VARIABLES

	public int balas;
	public int balasMax = 12;
	public int mochila = 60;


	// Use this for initialization
	void Start () {

		balas = balasMax;

	}
	
	// Update is called once per frame
	void Update () {

		if (Input.GetMouseButtonDown (0)) {
			if (balas > 0) {
				balas = balas - 1;
			}
		}

		if (Input.GetKeyUp ("r") ) {

			if ((mochila + balas) > 12) {
				mochila = mochila - (12 - balas);
				balas = balas + (-(balas - 12));
			} 
			else if ((mochila + balas) <= 12) {

				balas = balas + mochila;
				mochila = 0;

			}
		
	}
}
}
